package br.com.caelum.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

public class TesteProdutorFila {

	public static void main(String[] args) throws Exception {

		InitialContext context = new InitialContext(); // O lookup � feito atrav�s da classe InitialContext que se baseia no arquivo de configura��o jndi.properties
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
		
		Connection connection = factory.createConnection();
		connection.start();
		
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false significa que n�o quero uma transa��o e AUTO_ACKNOWLEDGE significa para automaticamente confirmar o recebimento de mensagem
		
		Destination fila = (Destination) context.lookup("financeiro"); // A nossa fila.financeiro � um Destination, � o lugar concreto onde a mensagem ser� salvo temporariamente
		
		MessageProducer producer = session.createProducer(fila);
		
		for(int i=0; i <= 1000; i++) {
			Message message = session.createTextMessage("<pedido><id>" + i + "</id></pedido>");
			producer.send(message);
		}
		
			
		session.close();
		connection.close();
		context.close();
	}

}
