package br.com.caelum.jms;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

public class TesteConsumidorFila {

	public static void main(String[] args) throws Exception {
		
		// Posso usar as duas formas de configura��o do contexto JNDI
		// Primeira forma
		// Properties properties = new Properties();
		// properties.setProperty("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");        
		// properties.setProperty("java.naming.provider.url", "tcp://192.168.0.94:61616");
		// properties.setProperty("queue.financeiro", "fila.financeiro");
		// InitialContext context = new InitialContext(properties);

		// Segunda forma
		InitialContext context = new InitialContext(); // O lookup � feito atrav�s da classe InitialContext que se baseia no arquivo de configura��o jndi.properties
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
		
		Connection connection = factory.createConnection();
		connection.start();
		
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false significa que n�o quero uma transa��o e AUTO_ACKNOWLEDGE significa para automaticamente confirmar o recebimento de mensagem
		
		Destination fila = (Destination) context.lookup("financeiro"); // A nossa fila.financeiro � um Destination, � o lugar concreto onde a mensagem ser� salvo temporariamente
		MessageConsumer consumer = session.createConsumer(fila);
		
		consumer.setMessageListener(new MessageListener() { // MessageListener serve para o consumer ficar escutando o tempo todo e nunca terminar quando receber uma mensagem
			
			@Override
			public void onMessage(Message message) {
				TextMessage textMessage = (TextMessage) message;
				
				try {
					System.out.println(textMessage.getText());
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
		
		new Scanner(System.in).nextLine();
		
		session.close();
		connection.close();
		context.close();
	}

}
