package br.com.caelum.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

import br.com.caelum.jms.modelo.Pedido;
import br.com.caelum.jms.modelo.PedidoFactory;

public class TesteProdutorTopico {

	public static void main(String[] args) throws Exception {

		InitialContext context = new InitialContext(); // O lookup � feito atrav�s da classe InitialContext que se baseia no arquivo de configura��o jndi.properties
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
		
		Connection connection = factory.createConnection();
		connection.start();
		
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false significa que n�o quero uma transa��o e AUTO_ACKNOWLEDGE significa para automaticamente confirmar o recebimento de mensagem
		
		Destination topico = (Destination) context.lookup("loja"); // A nossa fila.financeiro � um Destination, � o lugar concreto onde a mensagem ser� salvo temporariamente
		
		MessageProducer producer = session.createProducer(topico);
		
		Pedido pedido = new PedidoFactory().geraPedidoComValores();
		
//		StringWriter writer = new StringWriter();
//		JAXB.marshal(pedido, writer);
//		String xml = writer.toString();
//		System.out.println(xml);
		
		Message message = session.createObjectMessage(pedido);
		message.setBooleanProperty("ebook", false);
		producer.send(message);
		
			
		session.close();
		connection.close();
		context.close();
	}

}
