package br.com.caelum.jms;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.InitialContext;

import br.com.caelum.jms.modelo.Pedido;

public class TesteConsumidorTopicoComercial {

	public static void main(String[] args) throws Exception {
		
		// Posso usar as duas formas de configura��o do contexto JNDI
		// Primeira forma
		// Properties properties = new Properties();
		// properties.setProperty("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");        
		// properties.setProperty("java.naming.provider.url", "tcp://192.168.0.94:61616");
		// properties.setProperty("queue.financeiro", "fila.financeiro");
		// InitialContext context = new InitialContext(properties);

		// Segunda forma
		InitialContext context = new InitialContext(); // O lookup � feito atrav�s da classe InitialContext que se baseia no arquivo de configura��o jndi.properties
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
		
		Connection connection = factory.createConnection();
		connection.setClientID("comercial");
		
		connection.start();
		
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false significa que n�o quero uma transa��o e AUTO_ACKNOWLEDGE significa para automaticamente confirmar o recebimento de mensagem
		
		Topic topico = (Topic) context.lookup("loja"); // O t�pico envia uma mesma mensagem para diversos consumidores simultaneamente
		
		/*
		 * Assinaturas dur�veis s� existem para t�picos. Uma assinatura dur�vel � nada
		 * mais do que um consumidor de um t�pico que se identificou. Ou seja, o t�pico
		 * sabe da exist�ncia desse consumidor.
		 * 
		 * O t�pico, por padr�o, n�o garante a entrega da mensagem, pois n�o sabe se
		 * existe 1 ou 20 consumidores. Ent�o de cara, o t�pico s� entrega as mensagens
		 * para consumidores que estiverem online.
		 * 
		 * Podemos mudar isso atrav�s de assinaturas dur�veis. Isso � nada mais do que
		 * um consumidor identificado.
		 * 
		 */
		MessageConsumer consumer = session.createDurableSubscriber(topico, "assinatura-comercial");
		
		consumer.setMessageListener(new MessageListener() { // MessageListener serve para o consumer ficar escutando o tempo todo e nunca terminar quando receber uma mensagem
			
			@Override
			public void onMessage(Message message) {
				ObjectMessage objectMessage = (ObjectMessage) message;
				
				try {
					Pedido pedido = (Pedido) objectMessage.getObject();
					System.out.println(pedido);
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
		
		new Scanner(System.in).nextLine();
		
		session.close();
		connection.close();
		context.close();
	}

}
